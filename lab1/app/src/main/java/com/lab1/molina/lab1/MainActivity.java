package com.lab1.molina.lab1;

import android.content.ClipData;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;



public class MainActivity extends AppCompatActivity {
    ListView listView;
    ArrayList<ItemModel> listaObjetos;
    ItemModel x;
    ItemModel y;
    ItemModel z;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        listaObjetos= new ArrayList<>();
        x= new ItemModel("objeto 1", "soy un itemModel", null);
        y= new ItemModel("objeto 2", "soy itemModel", null);
        z= new ItemModel("objeto 3", "soy itemModel", null);

        listaObjetos.add(x);
        listaObjetos.add(y);
        listaObjetos.add(z);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.Lista);
        ArrayAdapter<ItemModel> adapter = new ArrayAdapter(this, android.R.layout.simple_expandable_list_item_1, listaObjetos );
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this,receiver.class);
                intent.putExtra("object",listaObjetos.get(i));

                startActivity(intent);

            }
        });

    }
}
