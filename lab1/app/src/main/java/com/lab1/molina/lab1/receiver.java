package com.lab1.molina.lab1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class receiver extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiver);

        Intent intent = getIntent();
        ItemModel objetoRecibido = intent.getParcelableExtra("object");
        String objH = objetoRecibido.getHeader();
        String objB= objetoRecibido.getBody();
        TextView textView = findViewById(R.id.txtHead);
        textView.setText(objH);
    }
}
