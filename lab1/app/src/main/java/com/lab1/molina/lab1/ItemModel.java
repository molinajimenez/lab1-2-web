package com.lab1.molina.lab1;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class ItemModel implements Parcelable {

    private final String header;
    private final String body;
    private final List<String> values;

    public ItemModel(String header, String body, List<String> values) {
        this.header = header;
        this.body = body;
        this.values = values;
    }

    protected ItemModel(Parcel in) {
        header = in.readString();
        body = in.readString();
        values = in.createStringArrayList();
    }

    public static final Creator<ItemModel> CREATOR = new Creator<ItemModel>() {
        @Override
        public ItemModel createFromParcel(Parcel in) {
            return new ItemModel(in);
        }

        @Override
        public ItemModel[] newArray(int size) {
            return new ItemModel[size];
        }
    };

    public String getHeader(){
        return this.header;
    }
    public String getBody(){
        return this.body;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(header);
        parcel.writeString(body);
        parcel.writeStringList(values);
    }
}
